use strum_macros::{Display, EnumString};

pub struct CouldNotAcquireScreen;

#[derive(Debug, EnumString, Display)]
pub enum WMError {
    CouldNotAcquireScreen(),
}
