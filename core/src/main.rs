use firewm::config::Config;
use firewm::config::KeyBindings;
use firewm::config::KeyCode;
use firewm::wm::run;
use firewm::wm::FireWM;
use std::process::exit;
use std::process::Command;

/**
 * penrose :: minimal configuration
 * This file will give you a functional if incredibly minimal window manager that has multiple
 * workspaces and simple client/workspace movement. For a more fleshed out example see the
 * 'simple_config_with_hooks' example.
 */

fn main() {
    let workspace_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
    Command::new("startx").spawn().unwrap();
    let mut key_bindings = KeyBindings::new();

    key_bindings.insert(
        KeyCode::Escape.to_string(),
        Box::new(|_wm| {
            Command::new("~/bin/myrmidon.sh")
                .arg("~/power-tasks.json5")
                .output()
                .unwrap();
        }),
    );
    key_bindings.insert(
        KeyCode::Enter.to_string(),
        Box::new(|_wm| {
            Command::new("alacritty").output().unwrap();
        }),
    );
    key_bindings.insert(
        KeyCode::Y.to_string(),
        Box::new(|_wm: &FireWM| {
            Command::new("shutdown").arg("now").output().unwrap();
        }),
    );

    let config = Config { keys: key_bindings };
    let wm = match FireWM::new(config) {
        Ok(x) => x,
        Err(e) => {
            println!("Error!: {}", e);
            exit(1);
        }
    };

    run(wm);
}
