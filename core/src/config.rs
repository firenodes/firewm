use crate::wm::FireWM;
use std::collections::HashMap;
use strum_macros::{Display, EnumString};
use xcb::xproto::Keycode;

/// Some action to be run by a user key binding
pub type KeyEventHandler = Box<dyn Fn(&FireWM)>;

/// User defined key bindings
pub type KeyBindings = HashMap<String, KeyEventHandler>;

#[derive(EnumString, Display)]
pub enum KeyCode {
    Escape,
    Enter,
    Y,
}

impl KeyCode {
    pub fn to_xcb(&self) -> Keycode {
        match self {
            KeyCode::Escape => 27,
            KeyCode::Enter => 13,
            KeyCode::Y => 89,
        }
    }
}

pub struct Config {
    pub keys: KeyBindings,
}
