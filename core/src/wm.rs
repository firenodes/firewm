use crate::config::Config;
use crate::config::KeyCode;
use crate::err::WMError;
use std::process::exit;
use std::str::FromStr;
use xcb;

pub struct FireWM {
    pub config: Config,
    pub connection: xcb::Connection,
    pub width: u16,
    pub height: u16,
    pub screen_num: i32,
    pub root: u32,
}

impl FireWM {
    pub fn new(config: Config) -> Result<FireWM, WMError> {
        let (connection, screen_num) = xcb::Connection::connect(None).unwrap();
        let setup = Self::setup(&connection, screen_num)?;

        println!("Screen Num: {}", screen_num);

        Ok(FireWM {
            config,
            connection,
            width: setup.0,
            height: setup.1,
            root: setup.2,
            screen_num,
        })
    }

    /* get width+height and the root screen */

    pub fn setup(
        ref connection: &xcb::Connection,
        screen_num: i32,
    ) -> Result<(u16, u16, u32), WMError> {
        let setup = connection.get_setup();
        if let Some(screen) = setup.roots().nth(screen_num as usize) {
            Ok((
                screen.width_in_pixels(),
                screen.height_in_pixels(),
                screen.root(),
            ))
        } else {
            Err(WMError::CouldNotAcquireScreen())
        }
    }

    pub fn init(&self) {
        self.enable_window_key_events(&self.root);
        let gc = self.connection.generate_id();
        let screen = self
            .connection
            .get_setup()
            .roots()
            .nth(self.screen_num as usize)
            .unwrap();
        xcb::create_gc(
            &self.connection,
            gc,
            screen.root(),
            &[
                (xcb::GC_FUNCTION, xcb::GX_XOR),
                (xcb::GC_FOREGROUND, screen.white_pixel()),
                (xcb::GC_BACKGROUND, screen.black_pixel()),
                (xcb::GC_LINE_WIDTH, 1),
                (xcb::GC_LINE_STYLE, xcb::LINE_STYLE_ON_OFF_DASH),
                (xcb::GC_GRAPHICS_EXPOSURES, 0),
            ],
        );

        xcb::poly_rectangle(
            &self.connection,
            screen.root(),
            gc,
            &[xcb::Rectangle::new(200, 200, 400, 400)],
        );

        xcb::map_window(&self.connection, screen.root());
        self.connection.flush();
    }

    pub fn enable_window_key_events(&self, window_id: &u32) {
        for k in self.config.keys.iter() {
            let key = KeyCode::from_str(k.0).unwrap();
            xcb::grab_key(
                &self.connection,
                false,
                *window_id,
                xcb::MOD_MASK_4 as u16,
                key.to_xcb(),
                xcb::GRAB_MODE_ASYNC as u8,
                xcb::GRAB_MODE_ASYNC as u8,
            );
        }
    }

    pub fn close_window(&self, window_id: &u32) {
        println!("Closing window {}", window_id);
        xcb::destroy_window(&self.connection, *window_id);
    }

    /*
     * Run the main event loop
     * Returns nothing on success, WMError if there is something
     * that went wrong
     */
    pub fn run(&self) -> Result<(), WMError> {
        let events = [(
            xcb::CW_EVENT_MASK,
            xcb::EVENT_MASK_BUTTON_PRESS
                | xcb::EVENT_MASK_BUTTON_RELEASE
                | xcb::EVENT_MASK_KEY_PRESS
                | xcb::EVENT_MASK_EXPOSURE
                | xcb::EVENT_MASK_SUBSTRUCTURE_REDIRECT
                | xcb::EVENT_MASK_SUBSTRUCTURE_NOTIFY,
        )];

        let cookie = xcb::change_window_attributes(&self.connection, self.root, &events);

        if !cookie.request_check().is_ok() {
            panic!("There's another Window Manager Running!");
        }

        self.connection.flush();

        'event_loop: loop {
            let event = self.connection.wait_for_event();
            self.init();
            match event {
                Some(e) => {
                    if self.match_event(e)? {
                        break 'event_loop;
                    }
                }
                None => {}
            };
        }
        Ok(())
    }
    fn match_event(&self, event: xcb::base::GenericEvent) -> Result<bool, WMError> {
        return match event.response_type() {
            xcb::KEY_PRESS => {
                let ev: &xcb::KeyPressEvent = unsafe { xcb::cast_event(&event) };
                println!("Key: {}", ev.detail());
                for k in self.config.keys.iter() {
                    let key = KeyCode::from_str(k.0).unwrap();
                    if key.to_xcb() == ev.detail() {
                        k.1(self);
                    }
                }
                Ok(false)
            }
            _ => {
                println!("Received some event: {}", event.response_type());
                Ok(false)
            }
        };
    }
}

/**
 * Helper function to print the result of whether or not the WM is running.
 */
pub fn run(ruwm: FireWM) {
    match ruwm.run() {
        Ok(_) => println!("{}", "Exit Success!"),
        Err(e) => {
            println!("{}", e);
            exit(1);
        }
    }
    exit(0);
}
